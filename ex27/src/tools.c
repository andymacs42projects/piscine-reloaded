#include <unistd.h>
#include "../head/ft_display_file.h"

void ft_putchar(char c)
{
	write(1, &c, 1);
}

void ft_putstr(char *str)
{
	int i;

	i = -1;
	while (str[++i])
		ft_putchar(str[i]);
}

int	ft_print_and_exit(char *str)
{
	ft_putstr(str);
	return (0);
}

int	ft_print_and_error_exit(char *str)
{
	ft_putstr(str);
	return (1);
}
