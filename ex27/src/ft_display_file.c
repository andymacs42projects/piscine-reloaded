#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "../head/ft_display_file.h"

#define BUF_SIZE 4096

int	main(int argc, char **argv)
{
	int fd;
	int ret;
	char buf[BUF_SIZE + 1];

	if (argc < 2)
		return (ft_print_and_exit("File name missing.\n"));
	if (argc > 2)
		return (ft_print_and_exit("Too many arguments.\n"));
	fd = open(argv[1], O_RDWR);
	if (fd == -1)
		return (ft_print_and_error_exit("The file can't be opened.\n"));
	ret = read(fd, buf, BUF_SIZE);
	buf[ret] = '\0';
	ft_putstr(buf);
	close(fd);
}
