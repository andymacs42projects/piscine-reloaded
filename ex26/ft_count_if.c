int	ft_count_if(char **tab, int(*f)(char*))
{
	int result;
	int iterator;

	result = 0;
	iterator = -1;
	while (tab[++iterator])
		if (f(tab[iterator]) == 1)
			result++;
}
